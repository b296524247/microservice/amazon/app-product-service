//package com.amazon.appproductservice.config;
//
//import org.springframework.http.server.reactive.ServerHttpRequest;
//import org.springframework.stereotype.Component;
//import org.springframework.web.server.ServerWebExchange;
//import org.springframework.web.server.WebFilter;
//import org.springframework.web.server.WebFilterChain;
//import reactor.core.publisher.Mono;
//import reactor.util.context.Context;
//
//@Component
//public class RequestFilter implements WebFilter {
//
//    public static final String WEB_REQUEST_ATTRIBUTES = "WebRequestAttributes";
//
//    @Override
//    public Mono<Void> filter(ServerWebExchange serverWebExchange, WebFilterChain webFilterChain) {
//        return webFilterChain.filter(serverWebExchange)
//                .subscriberContext(context -> context.put(WEB_REQUEST_ATTRIBUTES, serverWebExchange.getAttributes()));
//    }
//}