package com.amazon.appproductservice.aop;

import com.amazon.appproductservice.config.AuthClient;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.Set;

@Aspect
@Component
@RequiredArgsConstructor
public class CheckUserExecutor {

    private final AuthClient authClient;


    @Before(value = "@annotation(checkUser)")
    public Mono<Void> check(CheckUser checkUser) {
        return getServerHttpRequest().flatMap(serverHttpRequest -> {
            System.out.println("Qalay:");
            String authorization = serverHttpRequest.getHeaders().getFirst(HttpHeaders.AUTHORIZATION);
            if (authorization == null) {
                System.out.println(authorization);
                return Mono.error(RuntimeException::new);
            }
            return authClient.getUserMe(authorization)
                    .flatMap(userDTO -> {
                        if (!hasPermission(userDTO.getPermissions(), checkUser.permissions()))
                            return Mono.error(RuntimeException::new);
                        return Mono.empty();
                    });
        });
    }

    //[ADD,EDIT]
    //[EDIT, DELETE]
    private boolean hasPermission(Set<String> actualPermissions, String[] expectedPermission) {
        return Arrays.stream(expectedPermission).anyMatch(actualPermissions::contains);
    }

    public static final Class<ServerHttpRequest> CONTEXT_KEY = ServerHttpRequest.class;

//    public static Mono<ServerHttpRequest> getServerHttpRequest() {
//        System.out.println("GETSERVER WORKED");
////        return Mono.deferContextual(Mono::just).map(contextView -> contextView.get(ServerWebExchange.class).getRequest()).log();
//        return Mono.deferContextual(contextView -> Mono.just(contextView.get(CONTEXT_KEY)));
//    }

    public static Mono<ServerHttpRequest> getServerHttpRequest() {
//        return Mono.deferContextual(contextView ->
//                Mono.just(contextView.get(ServerWebExchange.class)
//                                .getRequest())
//                        .map(ServerWebExchange::getRequest)
//                        .map(ServerHttpRequest::class));

        return Mono.deferContextual(contextView -> {
            System.out.println("contextView = " + contextView);
            // Retrieve ServerWebExchange from the Reactor Context
            ServerWebExchange exchange = contextView.get(ServerWebExchange.class);
            ServerHttpRequest request = exchange.getRequest();
            return Mono.just(request);
        });
    }

}
